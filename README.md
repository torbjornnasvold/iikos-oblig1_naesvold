# Obligatorisk oppgave 1 - C-programmering med prosesser, tr�der og synkronisering

Denne oppgaven best�r av f�lgende laboppgaver fra kompendiet:

* 4.5.b (Lage nye prosesser og enkel synkronisering av disse)
* 5.6.a (Lage nye tr�der og enkel semafor-synkronisering av disse)
* 5.6.b (Flere Producere og Consumere)
* 6.10.a (Dining philosophers)

SE OPPGAVETEKST I KOMPENDIET. HUSK � REDIGER TEKSTEN NEDENFOR!

## Gruppemedlemmer

**TODO: Erstatt med navn p� gruppemedlemmene**

* Leif Torbj�rn N�svold

## Sjekkliste

* Har navnene p� gruppemedlemmene blitt skrevet inn over? | GJORT
* Har l�ringsassistenter og foreleser blitt lagt til med leserettigheter? | GJORT
* Er issue-tracker aktivert? | GJORT
* Er pipeline aktivert, og returnerer pipelinen "Successful"? | GJORT