#include <stdio.h>  /* printf */
#include <stdlib.h> /* exit */
#include <sys/wait.h> /* waitpid */
#include <unistd.h> /* fork */

void process(int number, int time) {
  printf("Prosess %d kjører\n", number);
  sleep(time);
  printf("Prosess %d kjørte i %d sekunder\n", number, time);
}

int main() {
    int p[6];

    // START P0
    // Fork en ny proc
    p[0] = fork();
    // Hvis barn:
    if(p[0] == 0) {
        // Kjør proc func
        process(0, 1);
        // Avslutt child
        exit(0);
    }

    // START P2
    // Fork en ny proc
    p[2] = fork();
    // Hvis barn:
    if(p[2] == 0) {
        // Kjør proc func
        process(2, 3);
        // Avslutt child
        exit(0);
    }
    // Vent på p(0)
    
    waitpid(p[0], NULL, 0);
    
    // START P1
    //Fork en ny proc
    p[1]=fork();
    // Hvis barn:
    if(p[1]==0) {
        // kjør proc func
        process(1, 2);
        // Avslutt child
        exit(0);
    }
    
    // Start P4
    // fork ny proc
    p[4]=fork();
    // hvis barn
    if(p[4]==0) {
        // kjør proc func
        process(4, 3);
        // avslutt child
        exit(0);
    }
    
  // VENT P1
    waitpid(p[1], NULL, 0);
    
  // VENT P2
    waitpid(p[2], NULL, 0);
    
  // START P3
    // fork en ny proc
    p[3]=fork();
    // hvis barn
    if(p[3]==0) {
        // kjør proc func
        process(3, 2);
        // avslutt child
        exit(0);
    }
    
  // VENT P4
    waitpid(p[4], NULL, 0);
    
  // START P5
    // fork en ny proc
    p[5]=fork();
    // hvis barn
    if(p[5]==0) {
        // kjør proc func
        process(5, 3);
        // avslutt child
        exit(0);
    }
    
  // VENT P3
    waitpid(p[3], NULL, 0);
    
  // VENT P5
    waitpid(p[5], NULL, 0);


  return 0;
}
